package com.jhipster.padbowl.game.infrastructure.secondary;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

interface GamesSpringRepository extends JpaRepository<GameEntity, UUID> {}
