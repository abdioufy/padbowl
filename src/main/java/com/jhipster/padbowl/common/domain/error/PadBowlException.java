package com.jhipster.padbowl.common.domain.error;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Parent exception used in PadBowl application. Those exceptions will be resolved as human readable errors.
 *
 * <p>
 * You can use this implementation directly:
 * </p>
 *
 * <p>
 * <code>
 *     <pre>
 *       PadBowlException.builder(StandardMessages.USER_MANDATORY)
 *          .argument("key", "value")
 *          .argument("other", "test")
 *          .status(ErrorsHttpStatus.BAD_REQUEST)
 *          .message("Error message")
 *          .cause(new RuntimeException())
 *          .build();
 *     </pre>
 *   </code>
 * </p>
 *
 * <p>
 * Or make extension exceptions:
 * </p>
 *
 * <p>
 * <code>
 *     <pre>
 *       public class MissingMandatoryValueException extends PadBowlException {
 *
 *         public MissingMandatoryValueException(PadBowlMessage padBowlMessage, String fieldName) {
 *           this(builder(padBowlMessage, fieldName, defaultMessage(fieldName)));
 *         }
 *
 *         protected MissingMandatoryValueException(PadBowlExceptionBuilder builder) {
 *           super(builder);
 *         }
 *
 *         private static PadBowlExceptionBuilder builder(PadBowlMessage padBowlMessage, String fieldName, String message) {
 *           return PadBowlException.builder(padBowlMessage)
 *               .status(ErrorsHttpStatus.INTERNAL_SERVER_ERROR)
 *               .argument("field", fieldName)
 *               .message(message);
 *         }
 *
 *         private static String defaultMessage(String fieldName) {
 *           return "The field \"" + fieldName + "\" is mandatory and wasn't set";
 *         }
 *       }
 *     </pre>
 *   </code>
 * </p>
 */
public class PadBowlException extends RuntimeException {
  private final Map<String, String> arguments;
  private final ErrorStatus status;
  private final PadBowlMessage padBowlMessage;

  protected PadBowlException(PadBowlExceptionBuilder builder) {
    super(getMessage(builder), getCause(builder));
    arguments = getArguments(builder);
    status = getStatus(builder);
    padBowlMessage = getPadBowlMessage(builder);
  }

  private static String getMessage(PadBowlExceptionBuilder builder) {
    if (builder == null) {
      return null;
    }

    return builder.message;
  }

  private static Throwable getCause(PadBowlExceptionBuilder builder) {
    if (builder == null) {
      return null;
    }

    return builder.cause;
  }

  private static Map<String, String> getArguments(PadBowlExceptionBuilder builder) {
    if (builder == null) {
      return null;
    }

    return Collections.unmodifiableMap(builder.arguments);
  }

  private static ErrorStatus getStatus(PadBowlExceptionBuilder builder) {
    if (builder == null) {
      return null;
    }

    return builder.status;
  }

  private static PadBowlMessage getPadBowlMessage(PadBowlExceptionBuilder builder) {
    if (builder == null) {
      return null;
    }

    return builder.padbowlMessage;
  }

  public static PadBowlExceptionBuilder builder(PadBowlMessage message) {
    return new PadBowlExceptionBuilder(message);
  }

  public Map<String, String> getArguments() {
    return arguments;
  }

  public ErrorStatus getStatus() {
    return status;
  }

  public PadBowlMessage getPadBowlMessage() {
    return padBowlMessage;
  }

  public static class PadBowlExceptionBuilder {
    private final Map<String, String> arguments = new HashMap<>();
    private String message;
    private ErrorStatus status;
    private PadBowlMessage padbowlMessage;
    private Throwable cause;

    public PadBowlExceptionBuilder(PadBowlMessage padBowlMessage) {
      this.padbowlMessage = padBowlMessage;
    }

    public PadBowlExceptionBuilder argument(String key, Object value) {
      arguments.put(key, getStringValue(value));

      return this;
    }

    private String getStringValue(Object value) {
      if (value == null) {
        return "null";
      }

      return value.toString();
    }

    public PadBowlExceptionBuilder status(ErrorStatus status) {
      this.status = status;

      return this;
    }

    public PadBowlExceptionBuilder message(String message) {
      this.message = message;

      return this;
    }

    public PadBowlExceptionBuilder cause(Throwable cause) {
      this.cause = cause;

      return this;
    }

    public PadBowlException build() {
      return new PadBowlException(this);
    }
  }
}
