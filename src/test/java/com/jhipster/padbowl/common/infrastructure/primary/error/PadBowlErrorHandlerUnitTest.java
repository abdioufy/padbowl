package com.jhipster.padbowl.common.infrastructure.primary.error;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import ch.qos.logback.classic.Level;
import com.jhipster.padbowl.common.LogSpy;
import com.jhipster.padbowl.common.domain.error.ErrorStatus;
import com.jhipster.padbowl.common.domain.error.PadBowlException;
import com.jhipster.padbowl.common.domain.error.StandardMessage;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

@ExtendWith({ SpringExtension.class, LogSpy.class })
class PadBowlErrorHandlerUnitTest {
  @Mock
  private MessageSource messages;

  @InjectMocks
  private PadBowlErrorHandler handler;

  private final LogSpy logs;

  public PadBowlErrorHandlerUnitTest(LogSpy logs) {
    this.logs = logs;
  }

  @BeforeClass
  public void loadUserLocale() {
    LocaleContextHolder.setLocale(Locale.FRANCE);
  }

  @Test
  public void shouldHandleAsServerErrorWithoutStatus() {
    ResponseEntity<PadBowlError> response = handler.handlePadBowlException(
      PadBowlException.builder(StandardMessage.USER_MANDATORY).build()
    );

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @Test
  void shouldHandleAllErrorsAsUniqueHttpCode() {
    Set<HttpStatus> statuses = new HashSet<>();

    for (ErrorStatus status : ErrorStatus.values()) {
      ResponseEntity<PadBowlError> response = handler.handlePadBowlException(
        PadBowlException.builder(StandardMessage.USER_MANDATORY).status(status).build()
      );

      statuses.add(response.getStatusCode());
    }

    assertThat(statuses.size()).isEqualTo(ErrorStatus.values().length);
  }

  @Test
  public void shouldGetDefaultPadBowlMessageWithoutMessageForBadRequest() {
    ResponseEntity<PadBowlError> response = handler.handlePadBowlException(
      PadBowlException.builder(null).status(ErrorStatus.BAD_REQUEST).build()
    );

    assertThat(response.getBody().getErrorType()).isEqualTo("user.bad-request");
  }

  @Test
  public void shouldGetDefaultPadBowlMessageWithoutMessageForServerError() {
    ResponseEntity<PadBowlError> response = handler.handlePadBowlException(PadBowlException.builder(null).build());

    assertThat(response.getBody().getErrorType()).isEqualTo("server.internal-server-error");
  }

  @Test
  public void shouldGetDefaultMessageForUnknownMessage() {
    when(messages.getMessage("padbowl.error.hey", null, Locale.ENGLISH)).thenThrow(new NoSuchMessageException("hey"));
    when(messages.getMessage("padbowl.error.server.internal-server-error", null, Locale.ENGLISH)).thenReturn("User message");
    ResponseEntity<PadBowlError> response = handler.handlePadBowlException(PadBowlException.builder(() -> "hey").build());

    PadBowlError body = response.getBody();
    assertThat(body.getErrorType()).isEqualTo("hey");
    assertThat(body.getMessage()).isEqualTo("User message");
  }

  @Test
  public void shouldHandleUserPadBowlExceptionWithMessageWithoutArguments() {
    RuntimeException cause = new RuntimeException();
    PadBowlException exception = PadBowlException
      .builder(StandardMessage.USER_MANDATORY)
      .cause(cause)
      .status(ErrorStatus.BAD_REQUEST)
      .message("Hum")
      .build();

    when(messages.getMessage("padbowl.error.user.mandatory", null, Locale.ENGLISH)).thenReturn("User message");
    ResponseEntity<PadBowlError> response = handler.handlePadBowlException(exception);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    PadBowlError body = response.getBody();
    assertThat(body.getErrorType()).isEqualTo("user.mandatory");
    assertThat(body.getMessage()).isEqualTo("User message");
    assertThat(body.getFieldsErrors()).isNull();
  }

  @Test
  public void shouldReplaceArgumentsValueFromPadBowlException() {
    when(messages.getMessage("padbowl.error.user.mandatory", null, Locale.ENGLISH))
      .thenReturn("User {{ firstName }} {{ lastName}} message");

    ResponseEntity<PadBowlError> response = handler.handlePadBowlException(
      PadBowlException.builder(StandardMessage.USER_MANDATORY).argument("firstName", "Joe").argument("lastName", "Dalton").build()
    );

    assertThat(response.getBody().getMessage()).isEqualTo("User Joe Dalton message");
  }

  @Test
  public void shouldHandleAsBadRequestForExceededSizeFileUpload() {
    when(messages.getMessage("padbowl.error.server.upload-too-big", null, Locale.ENGLISH))
      .thenReturn("The file is too big to be send to the server");

    ResponseEntity<PadBowlError> response = handler.handleFileSizeException(new MaxUploadSizeExceededException(1024 * 1024 * 30));

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody().getErrorType()).isEqualTo("server.upload-too-big");
    assertThat(response.getBody().getMessage()).isEqualTo("The file is too big to be send to the server");
  }

  @Test
  public void shouldHandleMethodArgumentTypeMismatchException() {
    MethodArgumentTypeMismatchException mismatchException = new MethodArgumentTypeMismatchException(null, null, null, null, null);

    ResponseEntity<PadBowlError> response = handler.handleMethodArgumentTypeMismatchException(mismatchException);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody().getErrorType()).isEqualTo("user.bad-request");
  }

  @Test
  public void shouldHandleMethodArgumentTypeMismatchExceptionWithPadBowlError() {
    RuntimeException cause = PadBowlException.builder(StandardMessage.USER_MANDATORY).build();
    MethodArgumentTypeMismatchException mismatchException = new MethodArgumentTypeMismatchException(null, null, null, null, cause);

    ResponseEntity<PadBowlError> response = handler.handleMethodArgumentTypeMismatchException(mismatchException);

    assertThat(response.getBody().getErrorType()).isEqualTo("user.mandatory");
  }

  @Test
  public void shouldLogUserErrorAsWarn() {
    handler.handlePadBowlException(
      PadBowlException.builder(StandardMessage.BAD_REQUEST).status(ErrorStatus.BAD_REQUEST).message("error message").build()
    );

    logs.assertLogged(Level.WARN, "error message");
  }

  @Test
  public void shouldLogAuthenticationExceptionAsDebug() {
    handler.handleAuthenticationException(new InsufficientAuthenticationException("oops"));

    logs.assertLogged(Level.DEBUG, "oops");
  }

  @Test
  public void shouldLogServerErrorAsError() {
    handler.handlePadBowlException(
      PadBowlException
        .builder(StandardMessage.INTERNAL_SERVER_ERROR)
        .status(ErrorStatus.INTERNAL_SERVER_ERROR)
        .message("error message")
        .build()
    );

    logs.assertLogged(Level.ERROR, "error message");
  }

  @Test
  public void shouldLogErrorResponseBody() {
    RestClientResponseException cause = new RestClientResponseException(
      "error",
      400,
      "status",
      null,
      "service error response".getBytes(),
      Charset.defaultCharset()
    );

    handler.handlePadBowlException(
      PadBowlException
        .builder(StandardMessage.INTERNAL_SERVER_ERROR)
        .status(ErrorStatus.INTERNAL_SERVER_ERROR)
        .message("error message")
        .cause(cause)
        .build()
    );

    logs.assertLogged(Level.ERROR, "error message");
    logs.assertLogged(Level.ERROR, "service error response");
  }
}
