package com.jhipster.padbowl.common.infrastructure.primary.error;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.infrastructure.primary.TestJson;
import java.util.List;
import org.junit.jupiter.api.Test;

class PadBowlErrorUnitTest {

  @Test
  public void shouldGetErrorInformation() {
    PadBowlFieldError fieldError = PadBowlFieldErrorUnitTest.defaultFieldError();
    PadBowlError error = defaultError(fieldError);

    assertThat(error.getErrorType()).isEqualTo("type");
    assertThat(error.getMessage()).isEqualTo("message");
    assertThat(error.getFieldsErrors()).containsExactly(fieldError);
  }

  @Test
  public void shouldSerializeToJson() {
    assertThat(TestJson.writeAsString(defaultError(PadBowlFieldErrorUnitTest.defaultFieldError()))).isEqualTo(defaultJson());
  }

  @Test
  void shouldDeserializeFromJson() {
    assertThat(TestJson.readFromJson(defaultJson(), PadBowlError.class))
      .usingRecursiveComparison()
      .isEqualTo(defaultError(PadBowlFieldErrorUnitTest.defaultFieldError()));
  }

  private String defaultJson() {
    return "{\"errorType\":\"type\",\"message\":\"message\",\"fieldsErrors\":[" + PadBowlFieldErrorUnitTest.defaultJson() + "]}";
  }

  private PadBowlError defaultError(PadBowlFieldError fieldError) {
    return new PadBowlError("type", "message", List.of(fieldError));
  }
}
