package com.jhipster.padbowl.common.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.cucumber.CucumberTestContext;
import com.jhipster.padbowl.cucumber.MockedSecurityContextRepository;
import com.jhipster.padbowl.security.AuthoritiesConstants;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuthenticationSteps {
  @Autowired
  private MockedSecurityContextRepository contexts;

  private static final Map<String, Authentication> USERS = Map.of(
    "admin",
    new TestingAuthenticationToken("admin", "N/A", AuthorityUtils.createAuthorityList(AuthoritiesConstants.ADMIN)),
    "Player2",
    new TestingAuthenticationToken("Player2", "N/A", AuthorityUtils.createAuthorityList(AuthoritiesConstants.PLAYER))
  );

  @Given("I am logged in as {string}")
  public void authenticateUser(String username) {
    Authentication authentication = USERS.get(username);
    SecurityContextHolder.getContext().setAuthentication(authentication);

    contexts.authentication(authentication);
  }

  @Given("I logout")
  @Given("I am not logged in")
  public void logout() {
    contexts.authentication(null);
  }

  @Then("I should get an authorization error")
  public void authorizationError() {
    assertThat(CucumberTestContext.getStatus()).isEqualTo(HttpStatus.FORBIDDEN);
  }
}
