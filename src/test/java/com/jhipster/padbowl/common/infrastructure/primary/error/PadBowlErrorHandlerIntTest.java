package com.jhipster.padbowl.common.infrastructure.primary.error;

import static org.assertj.core.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.jhipster.padbowl.common.PadBowlIntTest;
import com.jhipster.padbowl.common.infrastructure.primary.TestJson;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Locale;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@PadBowlIntTest
class PadBowlErrorHandlerIntTest {
  private static final Charset UTF_8 = Charset.forName("UTF-8");

  private static final String EN_TAG = Locale.ENGLISH.toLanguageTag();

  @Autowired
  private PadBowlErrorHandler exceptionTranslator;

  @Autowired
  private ErrorResource resource;

  private MockMvc mockMvc;

  @BeforeEach
  public void setup() {
    mockMvc = MockMvcBuilders.standaloneSetup(resource).setControllerAdvice(exceptionTranslator).build();
  }

  @Test
  public void shouldMapPadBowlExceptions() throws Exception {
    String response = mockMvc
      .perform(post(errorEndpoint("padbowl-exceptions")).header(HttpHeaders.ACCEPT_LANGUAGE, EN_TAG))
      .andExpect(status().isInternalServerError())
      .andReturn()
      .getResponse()
      .getContentAsString(UTF_8);

    assertThat(response).contains("An error occurs, our team is working to fix it!");
  }

  @Test
  void shouldMapResponseStatusWithMessageExceptions() throws UnsupportedEncodingException, Exception {
    String response = mockMvc
      .perform(post(errorEndpoint("responsestatus-with-message-exceptions")).header(HttpHeaders.ACCEPT_LANGUAGE, EN_TAG))
      .andExpect(status().isNotFound())
      .andReturn()
      .getResponse()
      .getContentAsString(UTF_8);

    assertThat(response).contains("oops");
  }

  @Test
  void shouldMapResponseStatusWithoutMessageExceptions() throws UnsupportedEncodingException, Exception {
    String response = mockMvc
      .perform(post(errorEndpoint("responsestatus-without-message-exceptions")).header(HttpHeaders.ACCEPT_LANGUAGE, EN_TAG))
      .andExpect(status().isNotFound())
      .andReturn()
      .getResponse()
      .getContentAsString(UTF_8);

    assertThat(response).contains("occured").contains("404");
  }

  @Test
  public void shouldGetMessagesInOtherLanguage() throws Exception {
    String response = mockMvc
      .perform(post(errorEndpoint("padbowl-exceptions")).header(HttpHeaders.ACCEPT_LANGUAGE, Locale.FRANCE.toLanguageTag()))
      .andExpect(status().isInternalServerError())
      .andReturn()
      .getResponse()
      .getContentAsString(UTF_8);

    assertThat(response).contains("Une erreur est survenue, notre équipe travaille à sa résolution !");
  }

  @Test
  public void shouldGetMessagesInDefaultLanguage() throws Exception {
    String response = mockMvc
      .perform(post(errorEndpoint("padbowl-exceptions")).header(HttpHeaders.ACCEPT_LANGUAGE, Locale.CHINESE.toLanguageTag()))
      .andExpect(status().isInternalServerError())
      .andReturn()
      .getResponse()
      .getContentAsString(UTF_8);

    assertThat(response).contains("An error occurs, our team is working to fix it!");
  }

  @Test
  public void shouldMapParametersBeanValidationErrors() throws Exception {
    String response = mockMvc
      .perform(get(errorEndpoint("oops")).header(HttpHeaders.ACCEPT_LANGUAGE, EN_TAG))
      .andExpect(status().isBadRequest())
      .andReturn()
      .getResponse()
      .getContentAsString(UTF_8);

    assertThat(response)
      .contains("The values you entered are incorrects.")
      .contains("The format is incorrect, it has to match \\\"complicated\\\".");
  }

  @Test
  public void shouldMapMinValueQueryStringBeanValidationErrors() throws Exception {
    String response = mockMvc
      .perform(get(errorEndpoint("?parameter=1")).header(HttpHeaders.ACCEPT_LANGUAGE, EN_TAG))
      .andExpect(status().isBadRequest())
      .andReturn()
      .getResponse()
      .getContentAsString(UTF_8);

    assertThat(response)
      .contains("The values you entered are incorrects.")
      .contains("The value you entered is too low, minimum autorized is 42.");
  }

  @Test
  public void shouldMapMaxValueQueryStringBeanValidationErrors() throws Exception {
    String response = mockMvc
      .perform(get(errorEndpoint("?parameter=100")).header(HttpHeaders.ACCEPT_LANGUAGE, EN_TAG))
      .andExpect(status().isBadRequest())
      .andReturn()
      .getResponse()
      .getContentAsString(UTF_8);

    assertThat(response)
      .contains("The values you entered are incorrects.")
      .contains("The value you entered is too high, maximum authorized is 42.");
  }

  @Test
  public void shouldMapBodyBeanValidationErrors() throws Exception {
    String response = mockMvc
      .perform(
        post(errorEndpoint("oops"))
          .header(HttpHeaders.ACCEPT_LANGUAGE, EN_TAG)
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestJson.writeAsString(new ComplicatedRequest("value")))
      )
      .andExpect(status().isBadRequest())
      .andReturn()
      .getResponse()
      .getContentAsString(UTF_8);

    assertThat(response)
      .contains("The values you entered are incorrects.")
      .contains("The format is incorrect, it has to match \\\"complicated\\\".");
  }

  @Test
  public void shouldMapTechnicalError() throws Exception {
    String response = mockMvc
      .perform(
        post(errorEndpoint("not-deserializables"))
          .header(HttpHeaders.ACCEPT_LANGUAGE, EN_TAG)
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestJson.writeAsString(new NotDeserializable("value")))
      )
      .andExpect(status().isInternalServerError())
      .andReturn()
      .getResponse()
      .getContentAsString(UTF_8);

    assertThat(response).contains("An error occurs, our team is working to fix it!");
  }

  @Test
  public void shouldHandleAccessDeniedException() throws Exception {
    String response = mockMvc
      .perform(get(errorEndpoint("access-denied")).header(HttpHeaders.ACCEPT_LANGUAGE, EN_TAG))
      .andExpect(status().isForbidden())
      .andReturn()
      .getResponse()
      .getContentAsString(UTF_8);

    assertThat(response).contains("You don't have sufficient rights to access this resource.");
  }

  @Test
  public void shouldHandleRuntimeException() throws Exception {
    String response = mockMvc
      .perform(get(errorEndpoint("runtime-exceptions")).header(HttpHeaders.ACCEPT_LANGUAGE, EN_TAG))
      .andExpect(status().isInternalServerError())
      .andReturn()
      .getResponse()
      .getContentAsString(UTF_8);

    assertThat(response).contains("An error occurs, our team is working to fix it!");
  }

  private URI errorEndpoint(String path) {
    try {
      return new URI("/errors/" + path);
    } catch (URISyntaxException e) {
      throw new AssertionError();
    }
  }
}
