package com.jhipster.padbowl.common.domain.error;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.domain.error.PadBowlException.PadBowlExceptionBuilder;
import org.junit.jupiter.api.Test;

class PadBowlExceptionUnitTest {

  @Test
  public void shouldGetUnmodifiableArguments() {
    assertThatThrownBy(() -> fullBuilder().build().getArguments().clear()).isExactlyInstanceOf(UnsupportedOperationException.class);
  }

  @Test
  public void shouldBuildWithoutBuilder() {
    PadBowlException exception = new PadBowlException(null);

    assertThat(exception.getCause()).isNull();
    assertThat(exception.getMessage()).isNull();
    assertThat(exception.getStatus()).isNull();
    assertThat(exception.getArguments()).isNull();
    assertThat(exception.getPadBowlMessage()).isNull();
  }

  @Test
  public void shouldGetExceptionInformation() {
    PadBowlException exception = fullBuilder().build();

    assertThat(exception.getArguments()).hasSize(2).contains(entry("key", "value"), entry("other", "test"));
    assertThat(exception.getStatus()).isEqualTo(ErrorStatus.BAD_REQUEST);
    assertThat(exception.getMessage()).isEqualTo("Error message");
    assertThat(exception.getPadBowlMessage()).isEqualTo(StandardMessage.USER_MANDATORY);
    assertThat(exception.getCause()).isExactlyInstanceOf(RuntimeException.class);
  }

  @Test
  void shouldMapNullArgumentAsNullString() {
    assertThat(fullBuilder().argument("nullable", null).build().getArguments().get("nullable")).isEqualTo("null");
  }

  @Test
  void shouldGetObjectsToString() {
    assertThat(fullBuilder().argument("object", 4).build().getArguments().get("object")).isEqualTo("4");
  }

  private PadBowlExceptionBuilder fullBuilder() {
    return PadBowlException
      .builder(StandardMessage.USER_MANDATORY)
      .argument("key", "value")
      .argument("other", "test")
      .status(ErrorStatus.BAD_REQUEST)
      .message("Error message")
      .cause(new RuntimeException());
  }
}
