package com.jhipster.padbowl.kipe;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
class SecuredTestService {

  @PreAuthorize("can('unknown', #dummy)")
  public boolean cannotMakeAnUnknownOperation(Dummy dummy) {
    return true;
  }
}
