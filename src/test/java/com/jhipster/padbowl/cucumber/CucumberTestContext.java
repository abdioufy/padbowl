package com.jhipster.padbowl.cucumber;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jhipster.padbowl.common.infrastructure.primary.TestJson;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Deque;
import java.util.Optional;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.Function;
import java.util.stream.Collectors;
import net.minidev.json.JSONArray;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

public final class CucumberTestContext {
  private static final Deque<RestQuery> queries = new ConcurrentLinkedDeque<>();
  private static JsonProvider jsonReader = Configuration.defaultConfiguration().jsonProvider();

  private CucumberTestContext() {}

  public static void addResponse(HttpRequest request, ClientHttpResponse response) {
    queries.addFirst(new RestQuery(request, response));
  }

  public static HttpStatus getStatus() {
    return queries.getFirst().getStatus();
  }

  public static <T> T getResponse(Class<T> responseClass) {
    return queries.getFirst().getResponse().map(response -> TestJson.readFromJson(response, responseClass)).orElse(null);
  }

  public static Object getElement(String jsonPath) {
    return queries.getFirst().getResponse().map(toElement(jsonPath)).orElse(null);
  }

  public static Object getElement(String uri, String jsonPath) {
    return queries
      .stream()
      .filter(query -> query.forUri(uri))
      .findFirst()
      .flatMap(response -> response.response.map(toElement(jsonPath)))
      .orElse(null);
  }

  private static Function<String, Object> toElement(String jsonPath) {
    return response -> {
      Object element = JsonPath.read(jsonReader.parse(response), jsonPath);

      if (element instanceof JSONArray) {
        JSONArray elements = (JSONArray) element;

        if (elements.size() == 0) {
          return null;
        }

        return elements.stream().map(Object::toString).collect(Collectors.joining(", "));
      }

      return element;
    };
  }

  public static String getCreatedWorkingFolderId() {
    return (String) CucumberTestContext.getElement("working-folders", "$.id");
  }

  public static void reset() {
    queries.clear();
  }

  private static class RestQuery {
    private final String uri;
    private final HttpStatus status;
    private final Optional<String> response;

    public RestQuery(HttpRequest request, ClientHttpResponse response) {
      uri = request.getURI().toString();
      try {
        status = response.getStatusCode();
        this.response = readResponse(response);
      } catch (IOException e) {
        throw new AssertionError(e.getMessage(), e);
      }
    }

    private Optional<String> readResponse(ClientHttpResponse response) throws IOException {
      try {
        return Optional.of(StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()));
      } catch (Exception e) {
        return Optional.empty();
      }
    }

    private boolean forUri(String uri) {
      return this.uri.contains(uri);
    }

    private HttpStatus getStatus() {
      return status;
    }

    private Optional<String> getResponse() {
      return response;
    }
  }
}
