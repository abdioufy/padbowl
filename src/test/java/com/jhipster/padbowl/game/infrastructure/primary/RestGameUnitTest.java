package com.jhipster.padbowl.game.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.infrastructure.primary.TestJson;
import com.jhipster.padbowl.game.domain.GamesFixture;
import org.junit.jupiter.api.Test;

class RestGameUnitTest {

  @Test
  void shouldConvertFromDomain() {
    assertThat(TestJson.writeAsString(RestGame.from(GamesFixture.threeRolls())))
      .startsWith("{\"id\":\"")
      .endsWith("\"player\":\"Player\"," + "\"score\":30,\"frames\":[{\"firstRoll\":10},{\"firstRoll\":6,\"secondRoll\":4}]}");
  }
}
