package com.jhipster.padbowl.web.rest;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.cucumber.CucumberTestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;

public class AccountsSteps {
  @Autowired
  private TestRestTemplate rest;

  @When("I get my account information")
  public void getAccountInforamtion() {
    rest.getForEntity("/api/account", Void.class);
  }

  @Then("My login should be {string}")
  public void shouldHaveLogin(String login) {
    assertThat(CucumberTestContext.getElement("$.login")).isEqualTo(login);
  }

  @Then("My email should be {string}")
  public void shouldHaveEmail(String email) {
    assertThat(CucumberTestContext.getElement("$.email")).isEqualTo(email);
  }
}
